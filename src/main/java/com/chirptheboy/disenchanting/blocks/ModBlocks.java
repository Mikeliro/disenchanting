package com.chirptheboy.disenchanting.blocks;

import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.registries.ObjectHolder;

public class ModBlocks {

    @ObjectHolder("disenchanting:disenchanter")
    public static Disenchanter DISENCHANTER;

    @ObjectHolder("disenchanting:disenchanter")
    public static TileEntityType<DisenchanterTile> DISENCHANTER_TILE;

    @ObjectHolder("disenchanting:disenchanter")
    public static ContainerType<DisenchanterContainer> DISENCHANTER_CONTAINER;

}
