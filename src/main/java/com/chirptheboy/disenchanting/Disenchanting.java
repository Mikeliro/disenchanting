package com.chirptheboy.disenchanting;

import com.chirptheboy.disenchanting.blocks.Disenchanter;
import com.chirptheboy.disenchanting.blocks.DisenchanterContainer;
import com.chirptheboy.disenchanting.blocks.DisenchanterTile;
import com.chirptheboy.disenchanting.blocks.ModBlocks;
import com.chirptheboy.disenchanting.setup.ClientProxy;
import com.chirptheboy.disenchanting.setup.IProxy;
import com.chirptheboy.disenchanting.setup.ModSetup;
import com.chirptheboy.disenchanting.setup.ServerProxy;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("disenchanting")
public class Disenchanting {

    public static final Logger LOGGER = LogManager.getLogger();

    public static final String MODID = "disenchanting";
    public static double RARITY_MULTIPLIER = 0.03F;
    public static double RARITY_OFFSET = 1.8F;
    public static int SLIDER_MULTIPLIER = 3;

    public static ModSetup setup = new ModSetup();

    public static IProxy proxy = DistExecutor.runForDist(() -> () -> new ClientProxy(), () -> () -> new ServerProxy());

    public Disenchanting() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);

        //Config.loadConfig(Config.CLIENT_CONFIG, FMLPaths.CONFIGDIR.get().resolve("disenchanter-client.toml"));
        Config.loadConfig(Config.COMMON_CONFIG, FMLPaths.CONFIGDIR.get().resolve("disenchanter-common.toml"));
    }

    private void setup(final FMLCommonSetupEvent event) {
        setup.init();
        proxy.init();
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {

        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> event) {
            event.getRegistry().register(new Disenchanter());
        }

        @SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> event) {
            Item.Properties properties = new Item.Properties()
                    .group(setup.itemGroup);
            event.getRegistry().register(new BlockItem(ModBlocks.DISENCHANTER, properties).setRegistryName("disenchanter"));
        }

        @SubscribeEvent
        public static void onTileEntityRegistry(final RegistryEvent.Register<TileEntityType<?>> event) {
            event.getRegistry().register(TileEntityType.Builder.create(DisenchanterTile::new, ModBlocks.DISENCHANTER).build(null).setRegistryName("disenchanter"));
        }

        @SubscribeEvent
        public static void onContainerRegistry(final RegistryEvent.Register<ContainerType<?>> event){
            event.getRegistry().register(IForgeContainerType.create((windowId, inv, data) -> {
                BlockPos pos = data.readBlockPos();
                return new DisenchanterContainer(windowId, Disenchanting.proxy.getClientWorld(), pos, inv, Disenchanting.proxy.getClientPlayer());
            }).setRegistryName("disenchanter"));
        }
    }
}
